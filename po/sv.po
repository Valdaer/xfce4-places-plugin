# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Daniel Nylander <po@danielnylander.se>, 2008
msgid ""
msgstr ""
"Project-Id-Version: Xfce Panel Plugins\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-04-22 12:32+0200\n"
"PO-Revision-Date: 2019-04-22 10:32+0000\n"
"Last-Translator: Nick Schermer <nick@xfce.org>\n"
"Language-Team: Swedish (http://www.transifex.com/xfce/xfce-panel-plugins/language/sv/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: sv\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. vim: set ai et tabstop=4:
#: ../panel-plugin/places.desktop.in.h:1 ../panel-plugin/cfg.c:100
#: ../panel-plugin/cfg.c:195 ../panel-plugin/cfg.c:396
msgid "Places"
msgstr "Platser"

#: ../panel-plugin/places.desktop.in.h:2
msgid "Access folders, documents, and removable media"
msgstr "Kom åt mappar, dokument och flyttbart media"

#. Trash
#: ../panel-plugin/model_system.c:152
msgid "Trash"
msgstr "Papperskorg"

#: ../panel-plugin/model_system.c:186
msgid "Desktop"
msgstr "Skrivbord"

#. File System (/)
#: ../panel-plugin/model_system.c:202
msgid "File System"
msgstr "Filsystem"

#. TRANSLATORS: this will result in "<path> on <hostname>"
#: ../panel-plugin/model_user.c:256
#, c-format
msgid "%s on %s"
msgstr "%s på %s"

#: ../panel-plugin/model_volumes.c:71
#, c-format
msgid "Failed to eject \"%s\""
msgstr "Misslyckades med att mata ut \"%s\""

#: ../panel-plugin/model_volumes.c:120
#, c-format
msgid "Failed to unmount \"%s\""
msgstr "Misslyckades med att avmontera \"%s\""

#: ../panel-plugin/model_volumes.c:170 ../panel-plugin/model_volumes.c:193
#, c-format
msgid "Failed to mount \"%s\""
msgstr "Misslyckades med att montera \"%s\""

#: ../panel-plugin/model_volumes.c:478
msgid "Mount and Open"
msgstr "Montera och öppna"

#: ../panel-plugin/model_volumes.c:491
msgid "Mount"
msgstr "Montera"

#: ../panel-plugin/model_volumes.c:511
msgid "Eject"
msgstr "Mata ut"

#: ../panel-plugin/model_volumes.c:521
msgid "Unmount"
msgstr "Avmontera"

#. TRANSLATORS: Please use the same translation here as in Thunar
#: ../panel-plugin/model_volumes_notify.c:128
msgid "Unmounting device"
msgstr "Monterar av enhet"

#. TRANSLATORS: Please use the same translation here as in Thunar
#: ../panel-plugin/model_volumes_notify.c:131
#, c-format
msgid ""
"The device \"%s\" is being unmounted by the system. Please do not remove the"
" media or disconnect the drive"
msgstr "Enheten \"%s\" avmonteras av systemet. Ta inte bort mediet eller koppla bort enheten."

#. TRANSLATORS: Please use the same translation here as in Thunar
#: ../panel-plugin/model_volumes_notify.c:138
#: ../panel-plugin/model_volumes_notify.c:262
msgid "Writing data to device"
msgstr "Skriver data till enhet"

#. TRANSLATORS: Please use the same translation here as in Thunar
#: ../panel-plugin/model_volumes_notify.c:141
#: ../panel-plugin/model_volumes_notify.c:265
#, c-format
msgid ""
"There is data that needs to be written to the device \"%s\" before it can be"
" removed. Please do not remove the media or disconnect the drive"
msgstr "Det finns data som behöver skrivas till enheten \"%s\" innan den kan tas bort. Ta inte bort mediet eller koppla från enheten. "

#. TRANSLATORS: Please use the same translation here as in Thunar
#: ../panel-plugin/model_volumes_notify.c:253
msgid "Ejecting device"
msgstr "matar ut enhet"

#. TRANSLATORS: Please use the same translation here as in Thunar
#: ../panel-plugin/model_volumes_notify.c:256
#, c-format
msgid "The device \"%s\" is being ejected. This may take some time"
msgstr "Enheten \"%s\" matas ut. Detta kan ta litet tid"

#: ../panel-plugin/view.c:656
msgid "Search for Files"
msgstr "Sök efter filer"

#: ../panel-plugin/view.c:697
msgid "_Clear"
msgstr "_Töm"

#. RECENT DOCUMENTS
#: ../panel-plugin/view.c:713 ../panel-plugin/cfg.c:555
msgid "Recent Documents"
msgstr "Senaste dokument"

#: ../panel-plugin/cfg.c:399
msgid "Close"
msgstr "Stäng"

#. BUTTON
#: ../panel-plugin/cfg.c:473
msgid "Button"
msgstr "Knapp"

#. BUTTON: Show Icon/Label
#: ../panel-plugin/cfg.c:478
msgid "_Show"
msgstr "_Visa"

#: ../panel-plugin/cfg.c:483
msgid "Icon Only"
msgstr "Endast ikon"

#: ../panel-plugin/cfg.c:484
msgid "Label Only"
msgstr "Endast etikett"

#: ../panel-plugin/cfg.c:485
msgid "Icon and Label"
msgstr "Ikon och etikett"

#. BUTTON: Label text entry
#: ../panel-plugin/cfg.c:494
msgid "_Label"
msgstr "_Etikett"

#. MENU
#: ../panel-plugin/cfg.c:506
msgid "Menu"
msgstr "Meny"

#. MENU: Show Icons
#: ../panel-plugin/cfg.c:511
msgid "Show _icons in menu"
msgstr "Visa _ikoner i meny"

#. MENU: Show Removable Media
#: ../panel-plugin/cfg.c:519
msgid "Show _removable media"
msgstr "Visa _flyttbart media"

#. MENU: - Mount and Open (indented)
#: ../panel-plugin/cfg.c:527
msgid "Mount and _Open on click"
msgstr "Montera och _öppna vid klick"

#. MENU: Show GTK Bookmarks
#: ../panel-plugin/cfg.c:539
msgid "Show GTK _bookmarks"
msgstr "Visa GTK-_bokmärken"

#. MENU: Show Recent Documents
#: ../panel-plugin/cfg.c:547
msgid "Show recent _documents"
msgstr "Visa senaste _dokument"

#. RECENT DOCUMENTS: Show clear option
#: ../panel-plugin/cfg.c:564
msgid "Show cl_ear option"
msgstr "Visa tö_mningsalternativ"

#. RECENT DOCUMENTS: Number to display
#: ../panel-plugin/cfg.c:576
msgid "_Number to display"
msgstr "Visa a_ntal"

#. SEARCH
#: ../panel-plugin/cfg.c:600
msgid "Search"
msgstr "Sök"

#. Search: command
#: ../panel-plugin/cfg.c:605
msgid "Co_mmand"
msgstr "Ko_mmando"

#: ../panel-plugin/support.c:155
msgid "Open"
msgstr "Öppna"

#: ../panel-plugin/support.c:170
msgid "Open Terminal Here"
msgstr "Öppna Terminal här"

#: ../panel-plugin/xfce4-popup-places.sh:28
msgid "Usage:"
msgstr "Användning:"

#: ../panel-plugin/xfce4-popup-places.sh:29
msgid "OPTION"
msgstr "ALTERNATIV"

#: ../panel-plugin/xfce4-popup-places.sh:31
msgid "Options:"
msgstr "Alternativ:"

#: ../panel-plugin/xfce4-popup-places.sh:32
msgid "Popup menu at current mouse position"
msgstr "Ppup-meny vid aktuell musposition"

#: ../panel-plugin/xfce4-popup-places.sh:33
msgid "Show help options"
msgstr "Visa hjälpalternativ"

#: ../panel-plugin/xfce4-popup-places.sh:34
msgid "Print version information and exit"
msgstr "Visa versionsinformation och avsluta"
